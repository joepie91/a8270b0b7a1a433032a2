db = getDatabaseConnectionSomehow();

app.use(function(req, res, next){
	req.db = db;
	next();
});

// [...]

app.get("/", function(req, res) {
	// req.db now contains the database connection
});